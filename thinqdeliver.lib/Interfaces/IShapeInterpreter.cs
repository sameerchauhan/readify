﻿using thinqdeliver.redhill.RedHill;

namespace thinqdeliver.lib.Interfaces
{
    public interface IShapeInterpreter
    {
        TriangleType WhatShapeIsThis(int angle, int angleTwo, int angleThree);
    }
}