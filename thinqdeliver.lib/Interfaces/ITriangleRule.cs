﻿using System.Collections.Generic;
using thinqdeliver.redhill.RedHill;

namespace thinqdeliver.lib.Interfaces
{
    public interface ITriangleRule
    {
        TriangleType GetTriangleType();
        bool IsMatch(List<int> sidesOrdered);
    }
}