namespace thinqdeliver.lib.Interfaces
{
    public interface IFibonacciCalculator
    {
        long FibonacciNumber(long number);
    }
}