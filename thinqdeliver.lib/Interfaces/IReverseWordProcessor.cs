﻿namespace thinqdeliver.lib.Interfaces
{
    public interface IReverseWordProcessor
    {
        string ReverseWords(string words);
    }
}