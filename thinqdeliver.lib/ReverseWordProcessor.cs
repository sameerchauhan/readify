﻿using System;
using System.Collections.Generic;
using System.Linq;
using thinqdeliver.lib.Interfaces;

namespace thinqdeliver.lib
{
    public class ReverseWordProcessor : IReverseWordProcessor
    {
        private const string Delimiter = " ";

        public string ReverseWords(string words)
        {
            if (words == null)
            {
                throw new ArgumentNullException("words cannot be null");
            }

            if (words.Trim() == String.Empty)
            {
                return words;
            }
            var w= words.Split(Delimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var reverseWords = new List<string>();
            foreach (var word in w)
            {
                var reverseWord = word.Reverse();
                reverseWords.Add(String.Join(string.Empty, reverseWord));
            }
            return string.Join(Delimiter, reverseWords);
        }
    }
}