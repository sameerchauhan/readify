﻿using System.Collections.Generic;
using thinqdeliver.lib.Interfaces;
using thinqdeliver.redhill.RedHill;

namespace thinqdeliver.lib
{
    public class ShapeInterpreter : IShapeInterpreter
    {
        private readonly List<ITriangleRule> _rules;

        public ShapeInterpreter(List<ITriangleRule> rules)
        {
            _rules = rules;
        }

        public TriangleType WhatShapeIsThis(int side, int sideB, int sideC)
        {
            if (side > 0 && sideB > 0 && sideC > 0)
            {
                var listOfAngles = new List<int> {side, sideB, sideC };
                foreach (var triangleRule in _rules)
                {
                    if (triangleRule.IsMatch(listOfAngles))
                    {
                        return triangleRule.GetTriangleType();
                    }
                }
            }
            return TriangleType.Error;
        }
    }
}