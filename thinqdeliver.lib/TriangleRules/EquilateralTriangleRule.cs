using System.Collections.Generic;
using System.Linq;
using thinqdeliver.lib.Interfaces;
using thinqdeliver.redhill.RedHill;

namespace thinqdeliver.lib.TriangleRules
{
    public class EquilateralTriangleRule : ITriangleRule
    {
        public TriangleType GetTriangleType()
        {
            return TriangleType.Equilateral;
        }

        public bool IsMatch(List<int> sides)
        {
            var countOfDistinct = sides.Distinct().Count();
            return countOfDistinct == 1;
        }
    }
}