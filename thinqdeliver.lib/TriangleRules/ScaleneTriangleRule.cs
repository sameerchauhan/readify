using System.Collections.Generic;
using System.Linq;
using thinqdeliver.lib.Interfaces;
using thinqdeliver.redhill.RedHill;

namespace thinqdeliver.lib.TriangleRules
{
    public class ScaleneTriangleRule : ITriangleRule
    {
        public TriangleType GetTriangleType()
        {
            return TriangleType.Scalene;
        }

        public bool IsMatch(List<int> sides)
        {
            if (sides.Distinct().Count() == 3)
            {
                var sidesOrdered = sides.OrderByDescending(r => r).ToList();
                return  (sidesOrdered[1] + sidesOrdered[2]) > sidesOrdered[0];
            }

            return false;
        }
    }
}