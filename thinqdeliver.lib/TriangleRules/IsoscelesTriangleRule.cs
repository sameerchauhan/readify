using System.Collections.Generic;
using System.Linq;
using thinqdeliver.lib.Interfaces;
using thinqdeliver.redhill.RedHill;

namespace thinqdeliver.lib.TriangleRules
{
    public class IsoscelesTriangleRule : ITriangleRule
    {
        public TriangleType GetTriangleType()
        {
            return TriangleType.Isosceles;
        }

        public bool IsMatch(List<int> sides)
        {
            if (sides.Distinct().Count() == 2)
            {
                var sidesOrdered = sides.OrderByDescending(r => r).ToList();
                return sidesOrdered[0] < (sidesOrdered[1]*2);
            }

            return false;
        }
    }
}