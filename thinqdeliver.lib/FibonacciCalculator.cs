using System;
using thinqdeliver.lib.Interfaces;

namespace thinqdeliver.lib
{
    public class FibonacciCalculator : IFibonacciCalculator
    {
        public long FibonacciNumber(long number)
        {
            var realNumber = number;
            //https://www.math.hmc.edu/funfacts/ffiles/10002.4-5.shtml
            if (number < 0 )
            {
                realNumber = realNumber*-1;
            }
            var goldenMean = (1 + Math.Sqrt(5))/2;
            var goldenNumber = (1 - Math.Sqrt(5)) / 2;
            var result = (Math.Pow(goldenMean, realNumber) - Math.Pow(goldenNumber, realNumber))/Math.Sqrt(5);
            var longResult = (long)result;
            if (number < 0 && number % 2 == 0)
            {
                longResult = longResult*-1;
            }

            return longResult;
        }
    }
}