﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using thinqdeliver.lib;
using thinqdeliver.lib.Interfaces;
using thinqdeliver.lib.TriangleRules;
using thinqdeliver.redhill.RedHill;

namespace thinqdeliver.wcf
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ThinqDeliverService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ThinqDeliverService.svc or ThinqDeliverService.svc.cs at the Solution Explorer and start debugging.
   
    public class ThinqDeliverService : IRedPill
    {
        private readonly IReverseWordProcessor _reverseWordProcessor;
        private readonly IShapeInterpreter _shapeInterpreter;
        private readonly IFibonacciCalculator _fibonacciCalculator;

        public ThinqDeliverService():this(new FibonacciCalculator(), 
            new ShapeInterpreter(new List<ITriangleRule>()
            {
                new EquilateralTriangleRule(),
                new IsoscelesTriangleRule(),
                new ScaleneTriangleRule()
            }), 
            new ReverseWordProcessor() )
        {

        }

        public ThinqDeliverService(IFibonacciCalculator fibonacciCalculator,
            IShapeInterpreter shapeInterpreter,
            IReverseWordProcessor reverseWordProcessor)
        {
            _fibonacciCalculator = fibonacciCalculator;
            _shapeInterpreter = shapeInterpreter;
            _reverseWordProcessor = reverseWordProcessor;
        }

        public Guid WhatIsYourToken()
        {
            return Guid.Parse("972a147b-25aa-4144-990f-51e481d8e94f");
        }

        public long FibonacciNumber(long number)
        {
            return _fibonacciCalculator.FibonacciNumber(number);
        }

        public TriangleType WhatShapeIsThis(int angle, int angleTwo, int angleThree)
        {
            return _shapeInterpreter.WhatShapeIsThis(angle, angleTwo, angleThree);
        }

        public string ReverseWords(string words)
        {
            return _reverseWordProcessor.ReverseWords(words);
        }
    }
}
