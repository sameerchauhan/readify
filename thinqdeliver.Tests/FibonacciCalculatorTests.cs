using System;
using NUnit.Framework;
using thinqdeliver.lib;

namespace thinqdeliver.Tests
{
    [TestFixture]
    public class FibonacciCalculatorTests
    {
        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 1)]
        [TestCase(3, 2)]
        [TestCase(4, 3)]
        [TestCase(5, 5)]
        [TestCase(6, 8)]
        [TestCase(12, 144)]
        [TestCase(-1,1)]
        [TestCase(-2,-1)]
        [TestCase(-3,2)]
        [TestCase(-4,-3)]
        [TestCase(-5,5)]
        public void ShouldReturnZeroWhenZeroIsEntered(long number, long expected)
        {
            var calculator = new FibonacciCalculator();
            var actual = calculator.FibonacciNumber(number);

            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}