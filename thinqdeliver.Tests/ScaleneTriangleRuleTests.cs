using System.Collections.Generic;
using NUnit.Framework;
using thinqdeliver.lib;
using thinqdeliver.lib.TriangleRules;
using thinqdeliver.redhill.RedHill;

namespace thinqdeliver.Tests
{
    [TestFixture]
    public class ScaleneTriangleRuleTests
    {
        private readonly ScaleneTriangleRule _rule;

        public ScaleneTriangleRuleTests()
        {
            _rule = new ScaleneTriangleRule();
        }

        [TestCase(40, 25, 30)]
        [TestCase(25, 40, 30)]        
        public void ShouldReturnTrueMatches(int a, int b, int c)
        {
            var actual = _rule.IsMatch(new List<int> { a,b,c});

            Assert.That(actual, Is.True);
        }

        [TestCase(30, 60, 90)]
        [TestCase(30, 90, 60)]
        public void ShouldReturnFalseIfNotIsosceles(int a, int b, int c)
        {
            var actual = _rule.IsMatch(new List<int> { a, b, c });

            Assert.That(actual, Is.False);
        }

        [Test]
        public void ShouldReturnCorrectType()
        {
            var actual = _rule.GetTriangleType();

            Assert.That(actual, Is.EqualTo(TriangleType.Scalene));
        }
    }
}