﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using thinqdeliver.lib;
using thinqdeliver.lib.Interfaces;
using thinqdeliver.lib.TriangleRules;
using thinqdeliver.redhill.RedHill;
using thinqdeliver.wcf;


namespace thinqdeliver.Tests
{
    [TestFixture]
    public class ThinqDeliverServiceTest
    {
        private ThinqDeliverService _service;

        [SetUp]
        public void Setup()
        {
            _service = new ThinqDeliverService(new FibonacciCalculator(), new ShapeInterpreter(new List<ITriangleRule>
            {
                  new EquilateralTriangleRule(),
                new IsoscelesTriangleRule(),
                new ScaleneTriangleRule()
            }), new ReverseWordProcessor());
        }

        [Test]
        public void ShouldReturnToken()
        {
            var expected = Guid.Parse("972a147b-25aa-4144-990f-51e481d8e94f");

            var actual = _service.WhatIsYourToken();

            Assert.That(actual, Is.EqualTo(expected));
        }

        [TestCase(0, 0)]
        [TestCase(1, 1)]
        [TestCase(2, 1)]
        [TestCase(3, 2)]
        [TestCase(4, 3)]
        [TestCase(5, 5)]
        [TestCase(6, 8)]
        [TestCase(12, 144)]
        [TestCase(-1, 1)]
        [TestCase(-2, -1)]
        [TestCase(-3, 2)]
        [TestCase(-4, -3)]
        [TestCase(-5, 5)]
        public void ShouldReturnFibonacciNumber(long number, long expected)
        {
            var actual = _service.FibonacciNumber(number);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ShouldCallFibonacciCalculator()
        {
            var calculatorMock = new Mock<IFibonacciCalculator>();
            calculatorMock.Setup(calculator => calculator.FibonacciNumber(0)).Returns(0);
            var proxy = new ThinqDeliverService(calculatorMock.Object, null, null);
            proxy.FibonacciNumber(0);

            calculatorMock.Setup(calculator => calculator.FibonacciNumber(0));
        }

        [TestCase(60, 60, 60, TriangleType.Equilateral)]
        [TestCase(80, 45, 45, TriangleType.Isosceles)]
        [TestCase(65, 90, 30, TriangleType.Scalene)]
        [TestCase(60, 90, 30, TriangleType.Error)]
        public void ShouldTellWhatShape(int angle, int angleTwo, int angleThree, TriangleType type)
        {
            var actual = _service.WhatShapeIsThis(angle, angleTwo, angleThree);

            Assert.That(actual, Is.EqualTo(type));
        }

        [Test]
        public void ShouldCallShapeInterpreter()
        {
            var shapeInterpreterMock = new Mock<IShapeInterpreter>();
            shapeInterpreterMock.Setup(shapeInterpreter => shapeInterpreter.WhatShapeIsThis(60, 60, 60)).Returns(TriangleType.Equilateral);
            var proxy = new ThinqDeliverService(null, shapeInterpreterMock.Object, null);
            proxy.WhatShapeIsThis(60, 60, 60);

            shapeInterpreterMock.Verify(shapeInterpreter => shapeInterpreter.WhatShapeIsThis(60, 60, 60));
        }

        [Test]
        public void ShouldReverseWords()
        {
            var words = "Writing a test";
            var actual = _service.ReverseWords(words);
            Assert.That(actual, Is.EqualTo("gnitirW a tset"));
        }

        [Test]
        public void ShouldCallReverseWordProcessor()
        {
            var reverseWordProcessorMock = new Mock<IReverseWordProcessor>();
            reverseWordProcessorMock.Setup(reverseWordProcessor => reverseWordProcessor.ReverseWords(It.IsAny<string>()))
                .Returns(It.IsAny<string>());
            var proxy = new ThinqDeliverService(null, null, reverseWordProcessorMock.Object);
            proxy.ReverseWords(It.IsAny<string>());

            reverseWordProcessorMock.Verify(reverseWordProcessor => reverseWordProcessor.ReverseWords(It.IsAny<string>()));
        }
    }
}
