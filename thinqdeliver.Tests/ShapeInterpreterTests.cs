using System.Collections.Generic;
using NUnit.Framework;
using thinqdeliver.lib;
using thinqdeliver.lib.Interfaces;
using thinqdeliver.lib.TriangleRules;
using thinqdeliver.redhill.RedHill;

namespace thinqdeliver.Tests
{
    [TestFixture]
    public class ShapeInterpreterTests
    {
        private ShapeInterpreter _shapeInterpreter;

        [SetUp]
        public void Setup()
        {
            _shapeInterpreter = new ShapeInterpreter(new List<ITriangleRule>
            {
                new EquilateralTriangleRule(),
                new IsoscelesTriangleRule(),
                new ScaleneTriangleRule()
            });
        }

        [Test]
        public void ShouldInterpretEquilateral()
        {
            var shapeType = _shapeInterpreter.WhatShapeIsThis(60, 60, 60);

            Assert.That(shapeType, Is.EqualTo(TriangleType.Equilateral));
        }

        [Test]
        public void ShouldInterpretIsosceles()
        {
            var shapeType = _shapeInterpreter.WhatShapeIsThis(88, 45, 45);

            Assert.That(shapeType, Is.EqualTo(TriangleType.Isosceles));
        }

        [Test]
        public void ShouldInterpretScalene()
        {
            var shapeType = _shapeInterpreter.WhatShapeIsThis(30, 40, 25);

            Assert.That(shapeType, Is.EqualTo(TriangleType.Scalene));
        }


        [TestCase(-30,45,25)]
        [TestCase(30,-45,25)]
        [TestCase(30,45,-25)]
        [TestCase(-30,-45,-25)]
        public void ShouldReturnErrorForNegativeNumbers(int a, int b, int c)
        {
            var shapeType = _shapeInterpreter.WhatShapeIsThis(a,b,c);

            Assert.That(shapeType, Is.EqualTo(TriangleType.Error));
        }
    }
}