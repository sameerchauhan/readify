﻿using System;
using NUnit.Framework;
using thinqdeliver.lib;

namespace thinqdeliver.Tests
{
    [TestFixture]
    public class ReverseWordProcessorTests
    {
        [TestCase("Writing a test", "gnitirW a tset")]
        [TestCase(" ", " ")]
        public void ShouldReverseWords(string words, string expected)
        {
            var reverseWordProcessor = new ReverseWordProcessor();
            var actual = reverseWordProcessor.ReverseWords(words);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ShouldThrowNullException()
        {
            var reverseWordProcessor = new ReverseWordProcessor();
            Assert.Throws<ArgumentNullException>(() => reverseWordProcessor.ReverseWords(null));
        }
    }
}