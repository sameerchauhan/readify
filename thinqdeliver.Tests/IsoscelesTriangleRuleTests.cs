using System;
using System.Collections.Generic;
using NUnit.Framework;
using thinqdeliver.lib;
using thinqdeliver.lib.TriangleRules;
using thinqdeliver.redhill.RedHill;


namespace thinqdeliver.Tests
{

    [TestFixture]
    public class IsoscelesTriangleRuleTests
    {
        private readonly IsoscelesTriangleRule _rule;

        public IsoscelesTriangleRuleTests()
        {
            _rule = new IsoscelesTriangleRule();
        }

        [TestCase(300, 300, 599 )]
        [TestCase(599, 300, 300)]
        public void ShouldReturnTrueMatches(int a, int b, int c)
        {
            
            var actual = _rule.IsMatch(new List<int> { a,b,c});

            Assert.That(actual, Is.True);
        }

        [TestCase(90, 45, 45)]
        public void ShouldReturnFalseIfNotIsosceles(int a, int b, int c)
        {
            var actual = _rule.IsMatch(new List<int> { a, b, c });

            Assert.That(actual, Is.False);
        }

        [Test]
        public void ShouldReturnCorrectType()
        {
            var actual = _rule.GetTriangleType();

            Assert.That(actual, Is.EqualTo(TriangleType.Isosceles));
        }
    }
}